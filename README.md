# Unanet Search

This is a script enabling a more interactive and complete searchbar within the Unanet Timesheet screen.

# Installation Instructions

This script requires a browser extension called Tampermonkey to work. To download this extension, you must first follow the URL corresponding to what browser you are using.

+ [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
+ [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en)
+ [Microsoft Edge](https://www.microsoft.com/en-us/p/tampermonkey/9nblggh5162s?cid=msft_web_collection)
+ Internet Explorer is unfortunately not supported by Tampermonkey

Once you go to the link for your browser type, click the install button to add the extension.

Finally, you can go ahead and click the following link to reach the Tampermonkey script for starting timers in your Unanet Timesheet.
[Link](https://gitlab.com/davidbittner/unanet-timer/raw/master/unanet-timer.user.js)

Once you go to the link, a new tab should open from Tampermonkey. It will have an install button to the left of the screen. Go ahead and click that. A blank tab may open, go ahead and close it. 

You're done! The script should now work.

# Usage

You can now go to your Unanet Timesheet and properly search through the available projects. It works in the way a much more traditional search engine works in the sense that it does not require a full absolute match.
